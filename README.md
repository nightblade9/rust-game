# Rust Game

A 2D top-down shooter inspired by Amorphous+. Shoot enemies, absorb their powers, and use them to defeat more enemies!

Made in August 2019 (mostly) with Rust and Quicksilver.

#### [Try it here](https://nightblade9.gitlab.io/rust-game)
