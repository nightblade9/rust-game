use std::time::{Duration, Instant};

#[derive(Default)]
pub struct Delta(Duration);

impl Delta {
    pub fn new(duration: Duration) -> Delta {
        Delta(duration)
    }

    pub fn as_f64(&self) -> f64 {
        (self.0.as_secs() as f64) + ((self.0.subsec_nanos() as f64) / (1_000_000_000.0))
    }
}
