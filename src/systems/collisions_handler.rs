use crate::components::{Collision, Player, Position, Stun};
use specs::{Entities, ReadStorage, System, WriteStorage};

use ncollide2d::bounding_volume;
use ncollide2d::bounding_volume::bounding_volume::BoundingVolume;

use na;
use nalgebra::Isometry2;
use nalgebra::Vector2;

pub struct CollisionsHandler;

impl<'a> System<'a> for CollisionsHandler {
    type SystemData = (
        Entities<'a>,
        ReadStorage<'a, Position>,
        ReadStorage<'a, Collision>,
        ReadStorage<'a, Player>,
        WriteStorage<'a, Stun>,
    );

    fn run(
        &mut self,
        (entities, position_storage, collision_storage, player_storage,mut stun_storage): Self::SystemData,
    ) {
        use specs::Join;
        for (entity, position, collision, _) in (
            &*entities,
            &position_storage,
            &collision_storage,
            &player_storage,
        )
            .join()
        {
            let aabb = bounding_volume::aabb(
                &collision.shape,
                &Isometry2::new(Vector2::new(position.x, position.y), na::zero()),
            );
            for (collider_position, collider_collision, ()) in
                (&position_storage, &collision_storage, !&player_storage).join()
            {
                let collider_aab = bounding_volume::aabb(
                    &collider_collision.shape,
                    &Isometry2::new(
                        Vector2::new(collider_position.x, collider_position.y),
                        na::zero(),
                    ),
                );
                if aabb.intersects(&collider_aab) {
                    stun_storage.insert(entity, Stun::new()).unwrap();
                }
                break;
            }
        }
    }
}
