use specs::join::Join;
use specs::{Entities, ReadStorage, World, WriteStorage};
use specs::{Read, System};

use quicksilver::geom::{Rectangle, Vector};
use quicksilver::graphics::Color;
use quicksilver::lifecycle::Window;

use crate::components::{Position, Renderable, Size, Velocity, StaticMovement, Enemy, Collision};
use crate::renderer::{RenderableType, GameImages};
use crate::common::Delta;
use rand::rngs::ThreadRng;
use rand::{thread_rng, Rng};

use crate::statics::SCREEN_SIZE;
use nalgebra::Point2;
use ncollide2d::shape::ConvexPolygon;

pub struct BasicEnemySystem;

const SPAWN_CHANCE_PER_SECOND: f64 = 50.0;
const ENEMY_MOVEMENT_SPEED: f32 = 40.0;
const DIRECTION_WITHIN_ANGLE: f32 = 80.0;

impl<'a> System<'a> for BasicEnemySystem {
    type SystemData = (
        Entities<'a>,
        Read<'a, Delta>,
        WriteStorage<'a, Renderable>,
        WriteStorage<'a, Size>,
        WriteStorage<'a, Position>,
        WriteStorage<'a, Velocity>,
        WriteStorage<'a, StaticMovement>,
        WriteStorage<'a, Enemy>,
        WriteStorage<'a, Collision>,
    );

    fn run(
        &mut self,
        (
            entities,
            delta,
            mut renderables,
            mut sizes,
            mut positions,
            mut velocities,
            mut static_movements,
            mut enemies,
            mut collisions,
        ): Self::SystemData,
    ) {
        if rand::thread_rng().gen_range(0.0, 100.0) <= SPAWN_CHANCE_PER_SECOND * delta.as_f64() {
            let enemy = entities.create();
            // TODO: collisions

            let mut pos = Position { x: 0.0, y: 0.0 };
            let mut movement = StaticMovement { x: 1.0, y: 1.0 };
            let direction_angle_complement = 90.0 - DIRECTION_WITHIN_ANGLE;
            let angle;

            if rand::random() {
                // horizontal
                pos.y = rand::thread_rng().gen_range(0, SCREEN_SIZE.1) as f32;
                if rand::random() {
                    // left
                    pos.x = -10.0;
                    angle = rand::thread_rng().gen_range(
                        -90.0 + direction_angle_complement,
                        90.0 - direction_angle_complement,
                    );
                } else {
                    // right
                    pos.x = SCREEN_SIZE.0 as f32 + 10.0;
                    angle = rand::thread_rng().gen_range(
                        90.0 + direction_angle_complement,
                        270.0 - direction_angle_complement,
                    );
                }
            } else {
                // vertical
                pos.x = rand::thread_rng().gen_range(0, SCREEN_SIZE.0) as f32;
                if rand::random() {
                    // up
                    pos.y = -10.0;
                    angle = rand::thread_rng().gen_range(
                        direction_angle_complement,
                        180.0 - direction_angle_complement,
                    );
                } else {
                    // down
                    pos.y = SCREEN_SIZE.1 as f32 + 10.0;
                    angle = rand::thread_rng().gen_range(
                        180.0 + direction_angle_complement,
                        360.0 - direction_angle_complement,
                    );
                }
            }

            let direction = Vector::from_angle(angle);
            movement.x *= ENEMY_MOVEMENT_SPEED * direction.x;
            movement.y *= ENEMY_MOVEMENT_SPEED * direction.y;

            renderables.insert(enemy, Renderable {
                visible: true,
                renderable_type: RenderableType::Fixed(GameImages::BasicEnemy),
                layer: 0,
            });
            sizes.insert(enemy, Size { w: 84.0, h: 82.0 });
            sizes.insert(enemy, Size { h: 40.0, w: 40.0 });
            positions.insert(enemy, pos);
            velocities.insert(enemy, Velocity { x: 0.0, y: 0.0 });
            static_movements.insert(enemy, movement);
            enemies.insert(enemy, Enemy {});
            collisions.insert(
                enemy,
                Collision {
                    shape: ConvexPolygon::<f32>::try_from_points(&[
                        Point2::new(0.0, 0.0),
                        Point2::new(40.0, 0.0),
                        Point2::new(40., 0.),
                        Point2::new(40., 40.),
                    ])
                    .expect("Enemy collision creating problem"),
                },
            );
        }
    }
}
