use crate::components::{Position, Velocity};
use specs::{System, WriteStorage};

pub struct UpdatePosition;

const ACCEL_DECREASE: f32 = 10.;

impl<'a> System<'a> for UpdatePosition {
    type SystemData = (WriteStorage<'a, Velocity>, WriteStorage<'a, Position>);

    fn run(&mut self, (mut velocity, mut position): Self::SystemData) {
        use specs::Join;
        for (velocity, position) in (&mut velocity, &mut position).join() {
            // 0.05 should be delta
            position.x += velocity.x * 0.05;
            position.y += velocity.y * 0.05;
            if velocity.x > 0. {
                velocity.x -= ACCEL_DECREASE;
            } else if velocity.x < 0. {
                velocity.x += ACCEL_DECREASE;
            }
            if velocity.y > 0. {
                velocity.y -= ACCEL_DECREASE;
            } else if velocity.y < 0. {
                velocity.y += ACCEL_DECREASE;
            }
        }
    }
}
