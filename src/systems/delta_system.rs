use crate::common::Delta;
use crate::components::{Position, Velocity};
use specs::{System, Write, WriteStorage};
use std::time::Instant;

pub struct DeltaSystem(Instant);

impl DeltaSystem {
    pub fn new() -> DeltaSystem {
        DeltaSystem(Instant::now())
    }
}

impl<'a> System<'a> for DeltaSystem {
    type SystemData = Write<'a, Delta>;

    fn run(&mut self, mut delta: Self::SystemData) {
        *delta = Delta::new(Instant::now().duration_since(self.0));
        self.0 = Instant::now();
    }
}
