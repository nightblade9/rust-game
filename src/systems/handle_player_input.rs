use crate::components::{Player, Velocity};
use crate::quicksilver::input::Key;
use crate::PlayerInput;
use specs::{ReadStorage, System, WriteExpect, WriteStorage};
/// That system should handle all player input that stores in PlayerInput resource.
pub struct HandlePlayerInput;

const ACCEL: f32 = 20.;
const MAX_ACCEL: f32 = 100.;

impl<'a> System<'a> for HandlePlayerInput {
    type SystemData = (
        WriteExpect<'a, PlayerInput>,
        WriteStorage<'a, Velocity>,
        ReadStorage<'a, Player>,
    );

    fn run(&mut self, (mut player_inputs_vec, mut velocity, player): Self::SystemData) {
        use specs::Join;

        for (vel, _) in (&mut velocity, &player).join() {
            for input in player_inputs_vec.0.iter() {
                match input {
                    Key::W => {
                        let new_vel = vel.y - ACCEL;
                        if new_vel >= -MAX_ACCEL {
                            vel.y = new_vel;
                        }
                    }
                    Key::D => {
                        let new_vel = vel.x + ACCEL;
                        if new_vel <= MAX_ACCEL {
                            vel.x = new_vel;
                        }
                    }
                    Key::S => {
                        let new_vel = vel.y + ACCEL;
                        if new_vel <= MAX_ACCEL {
                            vel.y = new_vel;
                        }
                    }
                    Key::A => {
                        let new_vel = vel.x - ACCEL;
                        if new_vel >= -MAX_ACCEL {
                            vel.x = new_vel;
                        }
                    }
                    _ => {}
                }
            }
        }
        // After handle input vector we clear it.
        player_inputs_vec.0.clear();
    }
}
