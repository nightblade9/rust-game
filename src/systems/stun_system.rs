use crate::components::{Stun, Velocity};
use specs::{Entities, LazyUpdate, Read, System, WriteStorage};

pub struct StunSystem;

impl<'a> System<'a> for StunSystem {
    type SystemData = (
        Entities<'a>,
        WriteStorage<'a, Stun>,
        WriteStorage<'a, Velocity>,
        Read<'a, LazyUpdate>,
    );

    fn run(
        &mut self,
        (entity_storage, mut stun_storage, mut velocity_storage, updater): Self::SystemData,
    ) {
        use specs::join::*;
        for (entity, stun, velocity) in
            (&*entity_storage, &mut stun_storage, &mut velocity_storage).join()
        {
            if stun.time <= 0 {
                updater.remove::<Stun>(entity)
            } else {
                velocity.x = 0.;
                velocity.y = 0.;
                stun.time -= 1;
            }
        }
    }
}
