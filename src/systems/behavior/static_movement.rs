use crate::components::{Position, StaticMovement, Velocity};
use specs::{ReadStorage, System, WriteStorage};

pub struct StaticMovementSystem;

impl<'a> System<'a> for StaticMovementSystem {
    type SystemData = (ReadStorage<'a, StaticMovement>, WriteStorage<'a, Velocity>);

    fn run(&mut self, (static_movements, mut velocities): Self::SystemData) {
        use specs::Join;
        for (static_movement, velocity) in (&static_movements, &mut velocities).join() {
            velocity.x = static_movement.x;
            velocity.y = static_movement.y;
        }
    }
}
