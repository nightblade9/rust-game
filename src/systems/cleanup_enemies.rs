use crate::components::{Enemy, Position};
use crate::statics::SCREEN_SIZE;
use specs::join::Join;
use specs::{Entities, ReadStorage, System};

pub struct CleanupEnemiesSystem;

impl<'a> System<'a> for CleanupEnemiesSystem {
    type SystemData = (
        Entities<'a>,
        ReadStorage<'a, Enemy>,
        ReadStorage<'a, Position>,
    );

    fn run(&mut self, (entities, enemies, positions): Self::SystemData) {
        for (entity, enemy, position) in (&*entities, &enemies, &positions).join() {
            // check if offscreen
            if (position.x < 0.0 || position.x > SCREEN_SIZE.0 as f32)
                && (position.y < 0.0 || position.y > SCREEN_SIZE.1 as f32)
            {
                entities.delete(entity).expect("Problem while deleting entity");
            }
        }
    }
}
