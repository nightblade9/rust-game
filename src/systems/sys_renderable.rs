use crate::components::{Position, Renderable, Size};
use crate::renderer::{Renderer, RenderingOrder};
use quicksilver::geom::{Rectangle, Vector};
use specs::prelude::*;

pub struct SysRenderable;

impl<'a> System<'a> for SysRenderable {
    type SystemData = (
        Write<'a, Renderer>,
        ReadStorage<'a, Position>,
        ReadStorage<'a, Size>,
        ReadStorage<'a, Renderable>,
    );

    fn run(&mut self, (mut renderer, pos, size, r): Self::SystemData) {
        for (pos, size, r) in (&pos, &size, &r).join() {
            let rect = Rectangle {
                pos: Vector { x: pos.x, y: pos.y },
                size: Vector {
                    x: size.w,
                    y: size.h,
                },
            };
            renderer.add_order(RenderingOrder {
                rect,
                renderable_type: r.renderable_type.clone(),
                layer: r.layer,
            });
        }
    }
}
