extern crate quicksilver;

use quicksilver::{
    geom::Vector,
    graphics::{Background::Img, Color, Image},
    input::Key,
    lifecycle::{Asset, run, Event, Settings, State, Window},
    Error, Result,
};
use specs::{Builder, Dispatcher, DispatcherBuilder, World, WorldExt};
use renderer::{GameImages};

use crate::common::Delta;
use crate::components::{
    Collision, Enemy, Player, Position, Renderable, Size, StaticMovement, Stun, Velocity,
};
use crate::renderer::{RenderableType, Renderer};
use crate::resources::Assets;
use crate::statics::SCREEN_SIZE;
use crate::systems::{
    BasicEnemySystem, CleanupEnemiesSystem, CollisionsHandler, DeltaSystem, HandlePlayerInput,
    StaticMovementSystem, StunSystem, SysRenderable, UpdatePosition,
};

use nalgebra::Point2;
use ncollide2d::shape::ConvexPolygon;

mod common;
mod components;
mod renderer;
mod resources;
mod statics;
mod systems;

// That resource will storage player input every time. Later we surely need to change it from one value to vector of values
pub struct PlayerInput(Vec<Key>);

fn setup_ecs() -> World {
    let mut world = World::new();
    // Register all components storages
    world.register::<Position>();
    world.register::<Velocity>();
    world.register::<Renderable>();
    world.register::<Size>();
    world.register::<Player>();
    world.register::<StaticMovement>();
    world.register::<Enemy>();
    world.register::<Collision>();
    world.register::<Stun>();
    // Creating Player entity
    world
        .create_entity()
        .with(Renderable {
            visible: true,
            renderable_type: RenderableType::Rotated(GameImages::Player, 0),
            layer: 0,
        })
        .with(Size { w: 99.0, h: 45.0 })
        .with(Player {})
        .with(Position { x: 0., y: 0. })
        .with(Velocity { x: 0., y: 0. })
        .with(Collision {
            shape: ConvexPolygon::<f32>::try_from_points(&[
                Point2::new(0.0, 0.0),
                Point2::new(40.0, 0.0),
                Point2::new(40., 0.),
                Point2::new(40., 40.),
            ])
            .expect("Player collision creating problem"),
        })
        .build();

    let player_inputs_vec = PlayerInput(Vec::new());
    world.insert(player_inputs_vec);
    world.insert(Delta::default());
    world.maintain();
    world
}

struct Game {
    world: World,
    assets: Assets,
    update_dispatcher: Dispatcher<'static, 'static>,
    draw_dispatcher: Dispatcher<'static, 'static>,
}

impl State for Game {
    fn new() -> Result<Game> {
        // Load all asset images
        let mut game_assets = Assets::default();
        game_assets.add_image(GameImages::Player);
        game_assets.add_image(GameImages::BasicEnemy);

        // Dispatcher for render stuff
        let draw_dispatcher = DispatcherBuilder::new()
            .with(SysRenderable, "sys_renderable", &[])
            .build();

        // Other dispatcher for stuff in update fun
        let update_dispatcher = DispatcherBuilder::new()
            .with(HandlePlayerInput, "handle_player_input", &[])
            .with(StunSystem, "stun_system", &[])
            .with(UpdatePosition, "update_position", &["stun_system"])
            .with(CollisionsHandler, "collisions_handler", &[])
            .with(StaticMovementSystem, "static_movement", &[])
            .with(BasicEnemySystem, "basic_enemy_system", &[])
            .with(DeltaSystem::new(), "delta_system", &[])
            .with(CleanupEnemiesSystem, "cleanup_enemies", &[])
            .build();

        Ok(Self {
            world: setup_ecs(),
            assets: game_assets,
            update_dispatcher,
            draw_dispatcher,
        })
    }

    fn update(&mut self, _window: &mut Window) -> Result<()> {
        self.update_dispatcher.dispatch(&mut self.world);
        // Player input checking:
        // Maybe we can refactor this section to more beauty one
        if _window.keyboard()[Key::W].is_down() {
            let mut input = self.world.write_resource::<PlayerInput>();
            input.0.push(Key::W);
        }
        if _window.keyboard()[Key::D].is_down() {
            let mut input = self.world.write_resource::<PlayerInput>();
            input.0.push(Key::D);
        }
        if _window.keyboard()[Key::S].is_down() {
            let mut input = self.world.write_resource::<PlayerInput>();
            input.0.push(Key::S);
        }
        if _window.keyboard()[Key::A].is_down() {
            let mut input = self.world.write_resource::<PlayerInput>();
            input.0.push(Key::A);
        }
        if _window.keyboard()[Key::Escape].is_down() {
            _window.close();
        }
        // maintain() should be called everyframe for recording changes in ecs internal data sturctue(e.g if some entity was created/ deleted while runtime)
        self.world.maintain();
        Ok(())
    }

    fn event(&mut self, _event: &Event, _window: &mut Window) -> Result<()> {
        // TODO
        Ok(())
    }

    fn draw(&mut self, window: &mut Window) -> Result<()> {
        // draw gray background
        window.clear(Color::from_rgba(200, 200, 200, 1f32))?;

        self.world.insert(Renderer::default());

        self.draw_dispatcher.dispatch(&self.world);

        let mut renderer = self.world.write_resource::<Renderer>();
        renderer.render(window, &mut self.assets);
        Ok(())
    }

    fn handle_error(error: Error) {
        // TODO
        println!("quicksilver error {}", error);
    }
}

fn run_game() {
    // NOTE: Set HIDPI to 1.0 to get pixel-perfect rendering.
    // Otherwise the window resizes to whatever value the OS sets and
    // scales the contents.
    // https://docs.rs/glutin/0.19.0/glutin/dpi/index.html
    std::env::set_var("WINIT_HIDPI_FACTOR", "1.0");

    let settings = Settings {
        // If the graphics do need to be scaled (e.g. using
        // `with_center`), blur them. This looks better with fonts.
        scale: quicksilver::graphics::ImageScaleStrategy::Blur,
        ..Default::default()
    };
    // start the game
    run::<Game>("Captain Ahmad", Vector::new(SCREEN_SIZE.0, SCREEN_SIZE.1), settings);
}

fn main() {
    run_game();
}
