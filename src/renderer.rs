// Plagiarized from: https://github.com/AlisCode/ld44/blob/master/src/renderer.rs
use crate::resources::Assets;
use quicksilver::geom::Rectangle;
use quicksilver::graphics::{Background, Color};
use quicksilver::prelude::Window;
use std::cmp::Ord;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum GameImages {
    Player,
    BasicEnemy,
}

impl GameImages {
    pub fn get_path(&self) -> &str {
        match self {
            GameImages::Player => "player.png",
            GameImages::BasicEnemy => "basic-enemy.png",
        }
    }
}

pub struct RenderingOrder {
    pub rect: Rectangle,
    pub renderable_type: RenderableType,
    pub layer: u32,
}

#[derive(Debug, Clone)]
#[allow(dead_code)]
pub enum RenderableType {
    Fixed(GameImages),
    Rotated(GameImages, i32),
    Debug(Color),
}

impl RenderableType {
    pub fn render(&self, assets: &mut Assets, rect: &Rectangle, window: &mut Window) {
        match self {
            RenderableType::Debug(col) => window.draw(rect, Background::Col(*col)),
            RenderableType::Fixed(ldim) => assets.draw_image(window, rect, ldim),
            RenderableType::Rotated(ldim, deg) => {
                assets.draw_image_rotated(window, rect, ldim, *deg)
            }
        }
    }
}

#[derive(Default)]
pub struct Renderer {
    orders: Vec<RenderingOrder>,
    min_layer: u32,
    max_layer: u32,
}

impl Renderer {
    /// Adds an order to the renderer
    pub fn add_order(&mut self, order: RenderingOrder) {
        self.max_layer = self.max_layer.max(order.layer);
        self.min_layer = self.min_layer.min(order.layer);
        self.orders.push(order);
    }

    /// Renders every order on the window, lower layer = higher priority
    pub fn render(&mut self, window: &mut Window, assets: &mut Assets) {
        self.orders.sort_by(|a, b| a.layer.cmp(&b.layer));
        self.orders.iter().for_each(|o| {
            o.renderable_type.render(assets, &o.rect, window);
        });
    }
}

pub struct UIRenderingOrder {
    rectangle: Rectangle,
}

#[derive(Default)]
pub struct UIRenderer {
    orders: Vec<UIRenderingOrder>,
}
