use specs::{Component, DenseVecStorage};

#[derive(Debug)]
pub struct Size {
    pub h: f32,
    pub w: f32,
}

impl Component for Size {
    type Storage = DenseVecStorage<Self>;
}
