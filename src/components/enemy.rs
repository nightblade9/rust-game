use specs::{Component, DenseVecStorage};

#[derive(Debug)]
pub struct Enemy {}

impl Component for Enemy {
    type Storage = DenseVecStorage<Self>;
}
