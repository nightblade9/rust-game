use ncollide2d::shape::ConvexPolygon;
use specs::{Component, DenseVecStorage};

pub struct Collision {
    pub shape: ConvexPolygon<f32>,
}

impl Component for Collision {
    type Storage = DenseVecStorage<Self>;
}
