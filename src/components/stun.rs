use specs::{Component, DenseVecStorage};

#[derive(Debug)]
pub struct Stun {
    pub time: i16,
}

impl Stun {
    pub fn new()->Self{
        Stun {time: 50}
    }
}

impl Component for Stun {
    type Storage = DenseVecStorage<Self>;
}
