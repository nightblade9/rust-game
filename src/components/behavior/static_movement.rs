use specs::{Component, DenseVecStorage};

#[derive(Debug)]
pub struct StaticMovement {
    pub x: f32,
    pub y: f32,
}

impl Component for StaticMovement {
    type Storage = DenseVecStorage<Self>;
}
